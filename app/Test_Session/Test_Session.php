<?php

namespace App\Test_Session;

use Illuminate\Database\Eloquent\Model;

class Test_Session extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user-email',
    ];
}
