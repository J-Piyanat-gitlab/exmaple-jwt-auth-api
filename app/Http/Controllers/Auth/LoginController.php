<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use JWTException;

class LoginController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['login']]);
        Auth::shouldUse('api');
    }

    public function login(Request $request){
        $credentials = $request->all();
        try{
            if (! $token = auth()->attempt($credentials)) {
                return response()->json([
                    'status' => false,
                    'message' => 'invalid credentials !!'
                ], 401);
            }
        } catch (JWTException $e){
            return response()->json([
                'status' => false,
                'message' => 'could not create Token !!'
            ], 500);
        }
        return response()->json([
            'status' => true,
            'response' => Auth::user(),
            'usreType' => "user",
            'message' => 'Successfully login',
            'token' => $token
        ], 200);
    }

    public function check(Request $request){
        if(Auth::check()){
            return response()->json([
                'status' => true,
                'response' => Auth::user(),
                'usreType' => "user",
                'message' => 'Successfully check',
                'token' => $token
            ], 200);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Check Error !!'
            ], 500);
        }
    }
}
