<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\User;
use Auth;

class RegisterController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['login']]);
        Auth::shouldUse('api');
    }

    public function register(Request $request){
        $validate = $request->validate(
            [
                'name' => ['required', 'string', 'max:255', 'regex:/^[.\D]*$/'],
                // 'email' => ['required', 'string', 'email', 'max:255', 'unique:users', 'regex:/^[a-zA-Z0-9._-]+@[a-zA-Z]+(\.[a-zA-Z]{2,})$/'],
                // 'email' => ['required', 'string', 'max:255', 'unique:users', 'regex:/^[a-zA-Z0-9._-]+@[a-zA-Z]+(?:\.[a-zA-Z](?:[a-zA-Z]{2,}[a-zA-Z])?)/'],
                'email' => ['required', 'string', 'max:255', 'unique:users', 'regex:/^[a-zA-Z0-9._-]+@[a-zA-Z]+(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/'],
                // 'password' => ['required', 'string', 'min:8', 'regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^_~])(?=.*[0-9])(?=.*[\d\x]).*$/', 'confirmed'],
                'password' => ['required', 'string', 'min:8', 'regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%&*^_~-])(?=.*[0-9])(?=.*[\d\x]).*$/'],
                'password_confirmation' => ['required', 'same:password', 'min:8']
            ],
            [
                'name.required' => 'กรุณาระบุชื่อผู้ใช้',
                'name.regex' => 'ชื่อผู้ใช้ไม่ถูกต้อง',

                'email.required' => 'กรุณาระบุอีเมล !!',
                'email.unique' => 'อีเมลนี้มีผู้ใช้แล้ว !!',
                'email.regex' => 'อีเมลไม่ถูกต้อง',

                'password.required' => 'กรุณาระบุรหัสผ่านของคุณ',
                'password.min' => 'รหัสผ่านขั้นต่ำ 8 ตัว',
                'password.regex' => 'ต้องประกอบด้วยตัวอักษร(A-Z,a-z,0-9 และ!@#$%&*^_~-) อย่างละ 1 ตัว',

                'password_confirmation.required' => 'กรุณาระบุรหัสผ่านของคุณ',
                'password_confirmation.min' => 'รหัสผ่านขั้นต่ำ 8 ตัว',
                'password_confirmation.same' => 'รหัสผ่านไม่ตรงกัน',
            ]
        );

        $User = User::create([
            'name' => $request->name, 
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        return response()->json(['message' => 'register']);
    }
}
