<?php

namespace App\Http\Controllers\Test_Session;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Test_Session;

use Auth;

class Test_SessionController extends Controller
{
    public function index(){
        if(auth()->user()){
            return response()->json([
                'id' => Auth::user()->id,
                'email' => Auth::user()->email
            ]);
        }else{
            return response()->json([ 'message' => 'error login' ]);
        }
    }
}
