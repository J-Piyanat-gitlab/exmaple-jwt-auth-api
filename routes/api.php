<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    // 'prefix' => 'auth'
], function ($router) {
    Route::post('/register', 'Auth\RegisterController@register')->name('Register');
    Route::post('/login', 'Auth\LoginController@login')->name('Login');
    Route::get('/logout', function (){
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    });

    Route::get('/me', function () {
        if(auth()->user()){
            return response()->json([
                'id' => Auth::user()->id,
                'email' => Auth::user()->email
            ]);
        }else{
            return response()->json([ 'message' => 'error login' ]);
        }
    });
});

// Route::group([
//     'middleware' => 'admins',
//     // 'prefix' => 'auth'
// ], function ($router) {
//     Route::post('/adminregister', 'Auth\Admin\AdminRegisterController@register')->name('AdminRegister');
//     Route::post('/adminlogin', 'Auth\Admin\AdminLoginController@login')->name('AdminLogin');
//     Route::get('/logout', function (){
//         auth()->logout();
//         return response()->json(['message' => 'Successfully logged out']);
//     });

//     Route::get('/admin', function () {
//         if(auth()->user()){
//             return response()->json([
//                 'id' => Auth::user()->id,
//                 'email' => Auth::user()->email
//             ]);
//         }else{
//             return response()->json([ 'message' => 'error login' ]);
//         }
//     });
// });

Route::get('/index', 'Test_Session\Test_SessionController@index');

